import cv2
import numpy as np
import os, os.path

##img = cv2.imread("image.0132.jpg", cv2.IMREAD_GRAYSCALE)
##img = cv2.resize(img, (88, 128))
##print(img.shape)
##
##cv2.imshow("proba", img)



images_folder = 'A0033nii'
txt_folder = 'text4'

for image in os.listdir(images_folder):
    file_name = txt_folder + "/" + image[:-4] + ".txt"

    img_path = images_folder + "/" + image
    img = cv2.imread(img_path, cv2.IMREAD_GRAYSCALE)
    img = cv2.resize(img, (256, 256))

    
    height, width = img.shape
    with open(file_name, 'w') as file:
        file.write(str(height) + " " + str(width) + "\n")
        for i in range(height):
            for j in range(width):
                file.write(str(img[i][j]) + "\n")
