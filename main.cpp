// badprog.com
//#include <GL/glut.h>
#include <stdio.h>
#include <stdlib.h>
#include <freeglut.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <cstdlib>
#include <cmath>
#include <math.h>
#include <vector>


#include <glut.h>

#define FPS 60
#define TO_RADIANS 3.14/180.0

using namespace std;

//width and height of the window ( Aspect ration 16:9 )
const int width = 16*100;
const int height = 9*100;

float pitch = 0.0, yaw= 0.0;

void display();
void reshape(int w,int h);
void timer(int);

void passive_motion(int, int);
void camera();
float camX = 0.0, camZ = 0.0, camY = 0.0;

void keyboard(unsigned char key,int x,int y);
void keyboard_up(unsigned char key,int x,int y);


struct Motion
{
    bool Forward, Backward, Left, Right, Up, Down;
};

Motion motion = {false, false, false, false, false, false};



bool toggleMesh = true;
bool toggleField = false;
bool toggle_fast_render = false;
extern int triTable[256][16];

struct PointColor{
    float r; float g; float b;
};

struct Point3D {
    float x, y, z;
};
struct Triangle {
    Point3D point1;
    Point3D point2;
    Point3D point3;
};


std::vector<Triangle> TriangleVector;
float LIMIT = 0.5;


//const int sizeX = 10, sizeY = 10, sizeZ = 10;

const int sizeX = 512, sizeY = 180, sizeZ = 512;
//const int sizeX = 256, sizeY = 215, sizeZ = 176;

string path = "text/A0033/";
//string path = "text/sub-13-T1wnii/";

PointColor field[sizeX][sizeY][sizeZ];

void loadImagesToField(){

    for (int k = 0; k < sizeY; k++){

        string img_number = std::to_string((k+1));
        if (img_number.length() == 1){
            img_number = "000" + img_number;
        } else if (img_number.length() == 2) {
            img_number = "00" + img_number;
        } else if (img_number.length() == 3) {
            img_number = "0" + img_number;
        }


        string fileName = path + std::string("image.") + img_number + ".txt";

        cout << fileName <<endl;

        PointColor color;
        string pixelValue;
        float value;
        ifstream ReadFile(fileName);

        // first line contains shape of image
        getline(ReadFile, pixelValue);

        int i = 0;
        while (getline(ReadFile, pixelValue)) {
            
            value = (float)stoi(pixelValue) / 255;
            color.r = value;
            color.g = value;
            color.b = value;
            field[i / sizeZ][k][i % sizeZ] = color;
            i++;
        }
        ReadFile.close();
    }

    
}



void makeTriangles();

PointColor generatePointColor_RGB(){

    float r, g, b;
    r = ((double) rand() / (RAND_MAX));
    g = ((double) rand() / (RAND_MAX));
    b = ((double) rand() / (RAND_MAX));

    PointColor color;
    color.r = r;
    color.g = g;
    color.b = b;
   
    return color;
}

PointColor generatePointColor_BW(){

    float a;
    a = ((double) rand() / (RAND_MAX));

    PointColor color;
    color.r = a;
    color.g = a;
    color.b = a;
   
    return color;
}

void generateField(){
    for (int i = 0; i < sizeX; i++){
        for (int j = 0; j < sizeY; j++){
            for (int k = 0; k < sizeZ; k++){
                
            
                if ((i*10 - 50) * (i*10 - 50) + (j*10 - 50) * (j*10 - 50) + (k*10 - 50) * (k*10 - 50) < 40 * 40){
                    PointColor color;
                    color.r = 1; color.g = 1; color.b = 1;
                    field[i][j][k] = color;
                } else {
                    PointColor color;
                    color.r = 0.3; color.g = 0.3; color.b = 0.3;
                    field[i][j][k] = color;
                }

                //field[i][j][k] = generatePointColor_BW();

            }
        }
    }
}


void createSTL(){

    cout << "Creating STL file" << endl;
    ofstream STLFile("object.stl", ios::binary);

    // Write 80 bytes of header (can be any content, usually just 80 zeros)
    char header[80] = {0};
    STLFile.write(header, 80);

    // Write 4-byte unsigned integer representing the number of triangles
    uint32_t numTriangles = TriangleVector.size();
    STLFile.write(reinterpret_cast<char*>(&numTriangles), sizeof(numTriangles));

    Point3D temp;

    for (const auto& triangle : TriangleVector) {
        // Calculate the normal (in binary STL files, this is not optional)
        // Point3D v1 = {triangle.point2.x - triangle.point1.x, triangle.point2.y - triangle.point1.y, triangle.point2.z - triangle.point1.z};
        // Point3D v2 = {triangle.point3.x - triangle.point1.x, triangle.point3.y - triangle.point1.y, triangle.point3.z - triangle.point1.z};
        // Point3D normal = {
        //     v1.y * v2.z - v1.z * v2.y,
        //     v1.z * v2.x - v1.x * v2.z,
        //     v1.x * v2.y - v1.y * v2.x
        // };
        // float length = sqrt(normal.x * normal.x + normal.y * normal.y + normal.z * normal.z);
        // normal.x /= length;
        // normal.y /= length;
        // normal.z /= length;

        Point3D normal = {0,0,0};

        // Write normal
        STLFile.write(reinterpret_cast<char*>(&normal), sizeof(normal));

        // Write vertices

        temp = {-triangle.point1.x, triangle.point1.y, -triangle.point1.z};
        STLFile.write(reinterpret_cast<char*>(&temp), sizeof(temp));

        temp = {-triangle.point2.x, triangle.point2.y, -triangle.point2.z};
        STLFile.write(reinterpret_cast<char*>(&temp), sizeof(temp));

        temp = {-triangle.point3.x, triangle.point3.y, -triangle.point3.z};
        STLFile.write(reinterpret_cast<char*>(&temp), sizeof(temp));

        
        uint16_t attribute = 0;
        STLFile.write(reinterpret_cast<char*>(&attribute), sizeof(attribute));
    }

    // Close the file
    STLFile.close();
    cout << "Done!" << endl;
}

//int points[10][10][10];

void init() {
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    
    // cursor to screen and hide it
    glutWarpPointer(width/2,height/2);
    glutSetCursor(GLUT_CURSOR_NONE);

    // called each frame to calculate pitch and yaw
    glutPassiveMotionFunc(passive_motion);

    glutKeyboardFunc(keyboard);
    glutKeyboardUpFunc(keyboard_up);

    //generateField();
    loadImagesToField();
    makeTriangles();
    
}

int main(int argc,char**argv) {
    glutInit(&argc,argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(width, height);
    glutCreateWindow("Projectile Motion - 3D Simulation");

    init();
    glutDisplayFunc(display);
    glutReshapeFunc(reshape);
    glutTimerFunc(0,timer,0);    //more info about this is given below at definition of timer()

    glutMainLoop();
    return 0;
}

/* This function just draws the scene. I used Texture mapping to draw

   a chessboard like surface. If this is too complicated for you ,

   you can just use a simple quadrilateral */

void drawSphere(float x, float y, float z) {
    glPushMatrix();
    glTranslatef(-x, y, -z);
    glutSolidSphere(0.5, 2, 2);
    glPopMatrix();
}

void draw() {
    glEnable(GL_TEXTURE_2D);
    GLuint texture;
    glGenTextures(1,&texture);

    unsigned char texture_data[2][2][4] =
                    {
                        0,0,0,255,  255,255,255,255,
                        255,255,255,255,    0,0,0,255
                    };

    glBindTexture(GL_TEXTURE_2D,texture);
    glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,2,2,0,GL_RGBA,GL_UNSIGNED_BYTE,texture_data);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,
                    GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
                    GL_NEAREST);

    glBegin(GL_QUADS);

    glColor3f(1, 1, 1);
    glTexCoord2f(0.0,0.0);  glVertex3f(-50.0,-5.0,-50.0);
    glTexCoord2f(25.0,0.0);  glVertex3f(50.0,-5.0,-50.0);
    glTexCoord2f(25.0,25.0);  glVertex3f(50.0,-5.0,50.0);
    glTexCoord2f(0.0,25.0);  glVertex3f(-50.0,-5.0,50.0);

    glEnd();

    glDisable(GL_TEXTURE_2D);


    // glColor3f(1, 0, 0);
    // glBegin(GL_POINTS);      // GL_LINE_LOOP GL_TRIANGLES
        
    //     glVertex3i(0, 0, 0);
    //     glVertex3i(10, 0, 0);
    //     glVertex3i(0, 10, 0);
    
    // glEnd();


}

void drawField_distance_from_center() {
    
    float distance_from_center;
    float color;

    glColor3f(1, 0, 0);
    for (int i = 0; i < sizeX; i++){

        for (int j = 0; j < sizeY; j++){

            for (int k = 0; k < sizeZ; k++){
                
                distance_from_center = sqrt(pow(i, 2) + pow(j, 2) + pow(k, 2));
                color = (distance_from_center + 10) / (13+10);

                glColor3f(color, color, color);
                drawSphere(i*10, j*10, k*10);
            }
        }
    }
}

void drawField_rgb(){

    PointColor color;
    float scale = 1;
    for (int i = 0; i < sizeX; i++){

        for (int j = 0; j < sizeY; j++){

            for (int k = 0; k < sizeZ; k++){
                
                color = field[i][j][k];

                glColor3f(color.r, color.g, color.b);
                drawSphere(i*scale, j*scale, k*scale);
            }
        }
    }
    
}

void draw2x2x2(){
    int sizeX = 2, sizeY = 2, sizeZ = 2;

    float color;

    glColor3f(1, 0, 0);
    for (int i = 0; i < sizeX; i++){

        for (int j = 0; j < sizeY; j++){

            for (int k = 0; k < sizeZ; k++){   

                glColor3f(1, 1, 1);

                if (i == 0 && j == 0 && k == 1){
                    glColor3f(1, 0, 0);
                }
                if (i == 0 && j == 1 && k == 1){
                    glColor3f(1, 0, 0);
                }
                
                drawSphere(i*10, j*10, k*10);
            }
        }
    }

    glColor3f(0.5, 0.5, 0.5);
    glBegin(GL_LINES);

    glVertex3f(0,0,0); glVertex3f(-10,0,0); glVertex3f(-10,0,0); glVertex3f(-10,0,-10); glVertex3f(-10,0,-10); glVertex3f(0,0,-10); glVertex3f(0,0,-10); glVertex3f(0,0,0);
    
    glVertex3f(0,10,0); glVertex3f(-10,10,0); glVertex3f(-10,10,0); glVertex3f(-10,10,-10); glVertex3f(-10,10,-10); glVertex3f(0,10,-10); glVertex3f(0,10,-10); glVertex3f(0,10,0);

    glVertex3f(0,0,0); glVertex3f(0,10,0); glVertex3f(-10,0,0); glVertex3f(-10,10,0); glVertex3f(-10,0,-10); glVertex3f(-10,10,-10); glVertex3f(0,0,-10); glVertex3f(0,10,-10);

    glEnd();

}


Point3D calculateEdgeCoordinate(int i, int j, int k, int brid){

    Point3D point;
    point.x = i;        point.y = j;        point.z = k;

    if (brid == 0){
        point.x += 0.5; point.y += 0;       point.z += 1;
    }
    else if (brid == 1){
        point.x += 0;   point.y += 0;       point.z += 0.5;
    }
    else if (brid == 2){
        point.x += 0.5; point.y += 0;       point.z += 0;
    }
    else if (brid == 3){
        point.x += 1;   point.y += 0;       point.z += 0.5;
    }
    else if (brid == 4){
        point.x += 0.5; point.y += 1;       point.z += 1;
    }
    else if (brid == 5){
        point.x += 0;   point.y += 1;       point.z += 0.5;
    }
    else if (brid == 6){
        point.x += 0.5; point.y += 1;       point.z += 0;
    }
    else if (brid == 7){
        point.x += 1;   point.y += 1;       point.z += 0.5;
    }
    else if (brid == 8){
        point.x += 1;   point.y += 0.5;     point.z += 1;
    }
    else if (brid == 9){
        point.x += 0;   point.y += 0.5;     point.z += 1;
    }
    else if (brid == 10){
        point.x += 0;   point.y += 0.5;     point.z += 0;
    }
    else if (brid == 11){
        point.x += 1;   point.y += 0.5;     point.z += 0;
    }

    return point;
}

void makeTriangles(){

    TriangleVector.clear();

    PointColor point_0, point_1, point_2, point_3, point_4, point_5, point_6, point_7;
    bool point_0_b, point_1_b, point_2_b, point_3_b, point_4_b, point_5_b, point_6_b, point_7_b;

    for (int i = 0; i < sizeX - 1; i++){

        for (int j = 0; j < sizeY - 1; j++){

            for (int k = 0; k < sizeZ - 1; k++){   

                // i j k
                point_0 = field[i+1][j][k+1];   point_0_b = point_0.r > LIMIT ? true : false;
                point_1 = field[i][j][k+1];     point_1_b = point_1.r > LIMIT ? true : false;
                point_2 = field[i][j][k];       point_2_b = point_2.r > LIMIT ? true : false;
                point_3 = field[i+1][j][k];     point_3_b = point_3.r > LIMIT ? true : false;
                point_4 = field[i+1][j+1][k+1]; point_4_b = point_4.r > LIMIT ? true : false;
                point_5 = field[i][j+1][k+1];   point_5_b = point_5.r > LIMIT ? true : false;
                point_6 = field[i][j+1][k];     point_6_b = point_6.r > LIMIT ? true : false;
                point_7 = field[i+1][j+1][k];   point_7_b = point_7.r > LIMIT ? true : false;

                int row_index;
                row_index = 1 * point_0_b + 2 * point_1_b + 4 * point_2_b + 8 * point_3_b + 16 * point_4_b + 32 * point_5_b + 64 * point_6_b + 128 * point_7_b;
                //printf("%d\n", row_index);

                int brid1, brid2, brid3;

                Triangle trokut;
                for (int m = 0; m < 16; m = m + 3){
                    if (triTable[row_index][m] == -1) {
                        break;
                    }
                    brid1 = triTable[row_index][m];
                    brid2 = triTable[row_index][m+1];
                    brid3 = triTable[row_index][m+2];
                    Point3D point_1 = calculateEdgeCoordinate(i, j, k, brid1);
                    Point3D point_2 = calculateEdgeCoordinate(i, j, k, brid2);
                    Point3D point_3 = calculateEdgeCoordinate(i, j, k, brid3);

                    trokut.point1 = point_1;
                    trokut.point2 = point_2;
                    trokut.point3 = point_3;
                    
                    TriangleVector.push_back(trokut);
                }

            }
        }
    }
    cout << TriangleVector.size() << endl;
}

void drawMesh(){

    int fast_render = 1;
    if (toggle_fast_render) {
        fast_render = 10;
    }

    float scale = 1;
    Triangle trokut;

    
    glColor3f(0.5, 0.5, 0.5);
    glBegin(GL_TRIANGLES);

    for(int i = 0; i < TriangleVector.size(); i += 1 * fast_render){

        trokut = TriangleVector[i];

        glVertex3f(-trokut.point1.x * scale, trokut.point1.y * scale, -trokut.point1.z * scale);
        glVertex3f(-trokut.point2.x * scale, trokut.point2.y * scale, -trokut.point2.z * scale);
        glVertex3f(-trokut.point3.x * scale, trokut.point3.y * scale, -trokut.point3.z * scale);

    }

    glEnd();


    glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
    glColor3f(0.1, 0.1, 0.1);
    glBegin(GL_TRIANGLES);
    for(int i = 0; i < TriangleVector.size(); i += 1 * fast_render){

        trokut = TriangleVector[i];

        glVertex3f(-trokut.point1.x * scale, trokut.point1.y * scale, -trokut.point1.z * scale);
        glVertex3f(-trokut.point2.x * scale, trokut.point2.y * scale, -trokut.point2.z * scale);
        glVertex3f(-trokut.point3.x * scale, trokut.point3.y * scale, -trokut.point3.z * scale);

    }   
    glEnd();
    glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
}

void display() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
    camera();
    //draw();
    //drawField_distance_from_center();
    //drawField_rgb();
    //drawMesh();
    //draw2x2x2();
    if (toggleMesh){drawMesh();}
    if (toggleField){drawField_rgb();}
    

    glutSwapBuffers();
}

void reshape(int w,int h) {
    glViewport(0,0,w,h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(60,16.0/9.0,1,600);
    glMatrixMode(GL_MODELVIEW);

}

/*this funtion is used to keep calling the display function periodically 

  at a rate of FPS times in one second. The constant FPS is defined above and

  has the value of 60.

  This will keep updating the screen.

*/
void timer(int) {
    glutPostRedisplay();
    glutWarpPointer(width/2,height/2);
    glutTimerFunc(1000/FPS,timer,0);
}

void passive_motion(int x, int y) {

    int dev_x, dev_y;
    dev_x = (width / 2) - x;
    dev_y = (height / 2) - y;

    float sensitivity = 60.0;

    /* apply the changes to pitch and yaw*/
    yaw += (float) dev_x / sensitivity;
    pitch += (float) dev_y / sensitivity;
}

void camera(){

    float speed = 1.0;
    if(motion.Forward)
    {
        camX += cos((yaw+90)*TO_RADIANS) / speed;
        camZ -= sin((yaw+90)*TO_RADIANS) / speed;
    }
    if(motion.Backward)
    {
        camX += cos((yaw+90+180)*TO_RADIANS) / speed;
        camZ -= sin((yaw+90+180)*TO_RADIANS) / speed;
    }
    if(motion.Left)
    {
        camX += cos((yaw+90+90)*TO_RADIANS) / speed;
        camZ -= sin((yaw+90+90)*TO_RADIANS) / speed;
    }
    if(motion.Right)
    {
        camX += cos((yaw+90-90)*TO_RADIANS) / speed;
        camZ -= sin((yaw+90-90)*TO_RADIANS) / speed;
    }
    if(motion.Up)
    {
        camY += 1 / speed;
    }
    if(motion.Down)
    {
        camY -= 1 / speed;
    }

    /*limit the values of pitch 
      between -60 and 70
    */
    if(pitch >= 70)
        pitch = 70;
    if(pitch <= -60)
        pitch = -60;

    glRotatef(-pitch, 1.0, 0.0, 0.0);   // Along X axis
    glRotatef(-yaw, 0.0, 1.0, 0.0);     // Along Y axis

    glTranslatef(-camX, 0.0, -camZ);  // move world to camera

    glTranslatef(0.0, -camY ,0.0);
}

void keyboard(unsigned char key,int x,int y) {
    switch(key)
    {
    case 'W':
    case 'w':
        motion.Forward = true;
        break;
    case 'A':
    case 'a':
        motion.Left = true;
        break;
    case 'S':
    case 's':
        motion.Backward = true;
        break;
    case 'D':
    case 'd':
        motion.Right = true;
        break;
    case ' ':
        motion.Up = true;
        break;
    case 'Q':
    case 'q':
        motion.Down = true;
        break;
    case 't':
        toggleMesh = !toggleMesh;
        break;
    case 'r':
        toggleField = !toggleField;
        break;
    case 'f':
        toggle_fast_render = !toggle_fast_render;
        break;
    case 'g':
        LIMIT += 0.05;
        makeTriangles();
        cout << "LIMIT raised to " << LIMIT << endl;
        break;
    case 'h':
        LIMIT -= 0.05;
        makeTriangles();
        cout << "LIMIT lowered to " << LIMIT << endl;
        break;
    case 'o':
        createSTL();
        break;
    };
}

void keyboard_up(unsigned char key,int x,int y) {
    switch(key)
    {
    case 'W':
    case 'w':
        motion.Forward = false;
        break;
    case 'A':
    case 'a':
        motion.Left = false;
        break;
    case 'S':
    case 's':
        motion.Backward = false;
        break;
    case 'D':
    case 'd':
        motion.Right = false;
        break;
    case ' ':
        motion.Up = false;
        break;
    case 'Q':
    case 'q':
        motion.Down = false;
        break;
    }
}

